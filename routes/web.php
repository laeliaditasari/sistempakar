<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//ROUTE GROUP FOR ADMIN
//SEMUA ROUTE DI DALAM ROUTE INI DIAKSES DENGAN "/admin/..."
Route::prefix('admin')->namespace('Admin')->group(function () {
	Auth::routes();

	Route::middleware('auth')->group(function () {

		Route::get('/', 'DashboardController@index')->name('dashboard');

		Route::resource('/siswa', 'SiswaController');

		Route::resource('/indikator', 'IndikatorController');

		Route::resource('/pertanyaan', 'PertanyaanController');

		Route::get('/rekap', 'RekapController@index')->name('rekap.index');
		Route::get('/rekap/print', 'RekapController@printPDF')->name('rekap.print');
		Route::get('/rekap/{id}', 'RekapController@show')->name('rekap.show');
		Route::delete('/rekap/{id}', 'RekapController@destroy')->name('rekap.destroy');

	});

	
});



//ROUTE UNTUK FRONTEND

Route::name('siswa.')->group(function () {

	Route::get('/', 'MainController@index');

	Route::get('/login', 'LoginController@showLoginForm')->name('login');
	Route::post('/login', 'LoginController@siswaLogin');
	Route::get('/register', 'LoginController@showRegistrationForm')->name('register');
	Route::post('/register', 'LoginController@siswaRegister');

	Route::middleware('siswa')->group(function () {

		//ROUTE FRONTEND YANG HARUS LOGIN TERLEBIH DAHULU
		Route::get('/tes', 'MainController@tes')->name('tes');
		Route::post('/tes', 'MainController@simpanTes');

		Route::get('/hasil-tes', 'MainController@hasilTes')->name('hasil-tes');

		Route::post('/logout', 'LoginController@logout')->name('logout');
	});

});