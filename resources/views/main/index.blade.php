@extends('layouts.main')

@section('content')
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="http://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="http://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="http://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="row">
  <div class="col-lg-12 text-center">
    <br>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent dapibus cursus libero et commodo. Nullam congue non sem quis commodo. Fusce pretium magna posuere ante porta, ut pulvinar felis volutpat. Ut non eleifend lacus. Sed dictum, sem eget accumsan finibus, ipsum ipsum tristique nisl, sed tristique augue elit nec mauris. Praesent facilisis ullamcorper quam ut blandit. Donec facilisis maximus faucibus.</p>
    @if(Auth::guard('siswa')->check())
      <a href="{{ route('siswa.tes') }}" class="btn btn-success">Mulai Tes Sekarang</a>
    @else
      <a href="{{ route('siswa.login') }}" class="btn btn-primary">Login</a>
    @endif
  </div>
</div>
@endsection
