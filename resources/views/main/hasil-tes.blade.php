@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        @if($indikator)
        <h1><small>Gaya Belajar Anda</small> : <b><u>{{ $indikator->nama }} ({{ round($persentase['positif'], 2) }}%)</u></b></h1>
        <div class="text-center">
            @switch($indikator->nama)
                @case('Visual')
                    <img src="{{ asset('images/gaya-visual.png') }}">
                    @break

                @case('Auditorial')
                    <img src="{{ asset('images/gaya-auditorial.png') }}">
                    @break

                @case('Kinestetik')
                    <img src="{{ asset('images/gaya-kinestetik.png') }}">
                    @break
            @endswitch
        </div>
        <div>
            @if($persentase['positif'] > $persentase['negatif'])
            <p><b>Tentang Gaya Belajar {{ $indikator->nama }}</b></p>
            {!! $indikator->deskripsi !!}
            @else
            <p><b>Saran Untuk Anda</b></p>
            {!! $indikator->saran !!}
            @endif
        </div>
        @endif

        <div class="form-group text-center">
            <span>Tidak sesuai untuk anda ? </span>
            <a href="{{ route('siswa.tes') }}" class="btn btn-success btn-sm">Tes Ulang</a>
        </div>
    </div>
</div>
@endsection