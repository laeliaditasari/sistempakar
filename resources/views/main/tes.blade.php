@extends('layouts.main')

@section('css')
    @parent
    <style type="text/css">
        #pertanyaan th{
            text-align: center;
            vertical-align: middle;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/icheck-bootstrap/green.css') }}">
@endsection


@section('content')
<div>
    <p>Silahkan isi pertanyaan-pertanyaan berikut untuk mengetahui gaya belajar anda.</p>
</div>

<div class="row">
    <div class="col-md-12">
        <form method="POST" action="{{ route('siswa.tes') }}">
            @csrf
        <table id="pertanyaan" class="table table-bordered">
            <tr>
                <th rowspan="2" width="10px">No.</th>
                <th rowspan="2">Pertanyaan</th>
                <th colspan="4">Jawaban</th>
            </tr>
            <tr>
                <th width="100">Sangat Setuju</th>
                <th width="100">Setuju</th>
                <th width="100">Kurang Setuju</th>
                <th width="100">Tidak Setuju</th>
            </tr>
            @foreach($pertanyaans as $pertanyaan)
            <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td>{{ $pertanyaan->pertanyaan }} {{$pertanyaan->sifat}}</td>
                <td class="text-center"><input type="radio" name="jawaban[{{ $pertanyaan->id }}]" value="4"></td>
                <td class="text-center"><input type="radio" name="jawaban[{{ $pertanyaan->id }}]" value="3"></td>
                <td class="text-center"><input type="radio" name="jawaban[{{ $pertanyaan->id }}]" value="2"></td>
                <td class="text-center"><input type="radio" name="jawaban[{{ $pertanyaan->id }}]" value="1"></td>
            </tr>
            @endforeach
        </table>

        <div class="form-group text-right">
            <input type="submit" class="btn btn-success" value="Lihat Hasil Tes">
        </div>
        </form>
    </div>
</div>
@endsection



@section('js')
    @parent

    <script src="{{ asset('adminlte/plugins/icheck-bootstrap/icheck.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
                increaseArea: '20%' // optional
            });
        });
    </script>
@endsection
