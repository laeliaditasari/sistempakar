@csrf
<div class="form-group">
	<label for="nama" class="control-label">Nama Indikator</label>
	<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Indikator" value="{{ @$indikator->nama }}">
</div>

<div class="form-group">
	<label for="deskripsi" class=" control-label">Deskripsi : </label>
	<small>Muncul untuk siswa dengan indikator positif</small>
	<textarea class="textarea form-control" placeholder="Place some text here" name="deskripsi" 
				style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
					{{ @$indikator->deskripsi }}
				</textarea>
</div>

<div class="form-group">
	<label for="saran" class=" control-label">Saran : </label>
	<small>Muncul untuk siswa dengan indikator negatif</small>
	<textarea class="textarea form-control" placeholder="Place some text here" name="saran" 
				style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
					{{ @$indikator->saran }}
				</textarea>
</div>

<button type="submit" class="btn btn-info">Simpan</button>

@section('css')
@parent
	<!-- summernote -->
	<link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.css') }}">
@endsection

@section('js')
@parent
	<!-- Summernote -->
	<script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
	<script>
	  $(function () {
	    // Summernote
	    $('.textarea').summernote({
			height: 250
	    })
	  })
	</script>
@endsection