@extends('layouts.admin')

@section('title')
Indikator : {{ $indikator->nama }}
@endsection

@section('content')
<div>
    <a href="{{ route('indikator.edit', [$indikator]) }}" class="btn btn-primary">Edit Indikator</a>
    <a href="{{ route('indikator.destroy', [$indikator]) }}" 
        onclick="event.preventDefault(); if(confirm('Anda yakin ingin menghapus {{ $indikator->nama }} ?')) {
            document.getElementById('delete-indikator-{{ $indikator->id }}').submit();
        }"
        class="btn btn-danger">Hapus Indikator</a>

    <form id="delete-indikator-{{ $indikator->id }}" action="{{ route('indikator.destroy', [$indikator]) }}" method="POST" style="display: none;">
        @csrf
        @method('DELETE')
    </form>
</div>
<br>
<div class="row">
    <div class="col-lg-8 col-md-8">
        <table class="table bordered">
            <tr>
                <th style="min-width: 200px">Nama Indikator :</th>
                <td>{{ $indikator->nama }}</td>
            </tr>
            <tr>
                <th>Deskripsi :</th>
                <td>{!! $indikator->deskripsi !!}</td>
            </tr>
        </table>
    </div>
</div>
@endsection