@extends('layouts.admin')

@section('title', 'Tambah Indikator')

@section('content')
<div class="row">
    <div class="col-lg-6 col-md-6">
        <form action="{{ route('indikator.store') }}" method="POST">
            @include('admin.indikator._form')
        </form>
    </div>

    <div class="col-lg-6 col-md-6">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
@endsection