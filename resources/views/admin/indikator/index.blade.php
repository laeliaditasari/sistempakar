@extends('layouts.admin')

@section('title', 'Data Indikator')

@section('content')
<div>
    <a href="{{ route('indikator.create') }}" class="btn btn-success">Tambah Indikator</a>
</div>
<br><div class="card card-info">
    <div class="card-header">
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <table id="datatable" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Deskripsi</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($indikators as $indikator)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $indikator->nama }}</td>
                        <td>{{ str_limit(strip_tags($indikator->deskripsi), 50) }}</td>
                        <td>
                            <a href="{{ route('indikator.show', [$indikator]) }}" class="btn btn-sm btn-success"><i class="fas fa-eye"></i></a>
                            <a href="{{ route('indikator.edit', [$indikator]) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                            <a href="{{ route('indikator.destroy', [$indikator]) }}" 
                                onclick="event.preventDefault(); if(confirm('Anda yakin ingin menghapus indikator : {{ $indikator->nama }} ?')) {
                                    document.getElementById('delete-indikator-{{ $indikator->id }}').submit();
                                }"
                                class="btn btn-danger btn-sm">
                                <i class="fas fa-trash-alt"></i>
                            </a>

                            <form id="delete-indikator-{{ $indikator->id }}" action="{{ route('indikator.destroy', [$indikator]) }}" method="POST" style="display: none;">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


@section('css')
@parent
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.min.css') }}">
@endsection

@section('js')
@parent
    <!-- DataTable -->
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
          "columnDefs": [
            { "orderable": false, "targets": 3 }
          ]
        });
    } );
</script>
@endsection