@csrf

<div class="form-group">
	<label for="pertanyaan" class="control-label">Pertanyaan</label>
	<input type="text" class="form-control" id="pertanyaan" name="pertanyaan" placeholder="Pertanyaan" value="{{ @$pertanyaan->pertanyaan }}" autofocus>
</div>

<div class="form-group">
	<label for="indikator_id" class=" control-label">Indikator</label>
	<select id="indikator_id" name="indikator_id" class="form-control">
		<option value=""></option>
		@foreach($indikators as $indikator)
		<option value="{{ $indikator->id }}" {!! $indikator->id == @$pertanyaan->indikator_id ? 'selected' : '' !!}>
			{{ $indikator->nama }}</option>
		@endforeach
	</select>
</div>


<div class="form-group">
	<label for="sifat" class=" control-label">Sifat</label>
	<select id="sifat" name="sifat" class="form-control">
		<option value=""></option>
		<option value="positif" {!! @$pertanyaan->sifat == 'positif' ? 'selected' : '' !!}>Positif</option>
		<option value="negatif" {!! @$pertanyaan->sifat == 'negatif' ? 'selected' : '' !!}>Negatif</option>
	</select>
</div>

<button type="submit" class="btn btn-info">Simpan</button>