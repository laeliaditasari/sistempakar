@extends('layouts.admin')

@section('title')
Pertanyaan : {{ $pertanyaan->id }}
@endsection

@section('content')
<div>
    <a href="{{ route('pertanyaan.edit', [$pertanyaan]) }}" class="btn btn-primary">Edit Pertanyaan</a>
    <a href="{{ route('pertanyaan.destroy', [$pertanyaan]) }}" 
        onclick="event.preventDefault(); if(confirm('Anda yakin ingin menghapus pertanyaan {{ $pertanyaan->id }} ?')) {
            document.getElementById('delete-pertanyaan-{{ $pertanyaan->id }}').submit();
        }"
        class="btn btn-danger">Hapus Pertanyaan</a>

    <form id="delete-pertanyaan-{{ $pertanyaan->id }}" action="{{ route('pertanyaan.destroy', [$pertanyaan]) }}" method="POST" style="display: none;">
        @csrf
        @method('DELETE')
    </form>
</div>
<br>
<div class="row">
    <div class="col-lg-8 col-md-8">
        <table class="table bordered">
            <tr>
                <th style="min-width: 200px">Indikator :</th>
                <td>{{ $pertanyaan->indikator->nama }}</td>
            </tr>
            <tr>
                <th style="min-width: 200px">Pertanyaan :</th>
                <td>{{ $pertanyaan->pertanyaan }}</td>
            </tr>
            <tr>
                <th>Sifat :</th>
                <td>{{ $pertanyaan->sifat }}</td>
            </tr>
        </table>
    </div>
</div>
@endsection