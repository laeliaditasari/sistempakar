@extends('layouts.admin')

@section('title', 'Edit Pertanyaan')

@section('content')
<div class="row">
    <div class="col-lg-6">
        <form action="{{ route('pertanyaan.update', [$pertanyaan]) }}" method="POST">
            @method('PATCH')
            @include('admin.pertanyaan._form')
        </form>
    </div>

    <div class="col-lg-6">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
@endsection