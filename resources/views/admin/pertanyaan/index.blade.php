@extends('layouts.admin')

@section('title', 'Data Pertanyaan')

@section('content')
<div>
    <a href="{{ route('pertanyaan.create') }}" class="btn btn-success">Tambah Pertanyaan</a>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        @foreach($indikators as $indikator)
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">
                <small>Pertanyaan Indikator</small> : <b>{{$indikator->nama}} <small>({{count($indikator->pertanyaans)}})</small></b>
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="datatable-{{$indikator->nama}}" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
							<th>Kode</th>
                            <th>Pertanyaan</th>
                            <th style="width:130px">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($indikator->pertanyaans as $pertanyaan)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
							<td>{{ $pertanyaan->kode }}</td>
                            <td>{{ $pertanyaan->pertanyaan }} ({!! $pertanyaan->sifat == 'positif' ? '<span class="text-success"><b>+</b></span>' : '<span class="text-danger"><b>-</b></span>' !!})</td>
                            <td>
                                <a href="{{ route('pertanyaan.show', [$pertanyaan]) }}" class="btn btn-sm btn-success"><i class="fas fa-eye"></i></a>
                                <a href="{{ route('pertanyaan.edit', [$pertanyaan]) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                                <a href="{{ route('pertanyaan.destroy', [$pertanyaan]) }}" 
                                    onclick="event.preventDefault(); if(confirm('Anda yakin ingin menghapus pertanyaan ini ?')) {
                                        document.getElementById('delete-pertanyaan-{{ $pertanyaan->id }}').submit();
                                    }"
                                    class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash-alt"></i>
                                </a>

                                <form id="delete-pertanyaan-{{ $pertanyaan->id }}" action="{{ route('pertanyaan.destroy', [$pertanyaan]) }}" method="POST" style="display: none;">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
            <!-- /.card-body -->
        </div>
        <br>
        <br>
        @endforeach
    </div>
</div>

@endsection



@section('css')
@parent
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.min.css') }}">
@endsection

@section('js')
@parent
    <!-- DataTable -->
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        @foreach($indikators as $indikator)
            $('#datatable-{{$indikator->nama}}').DataTable({
              "columnDefs": [
                { "orderable": false, "targets": 3 }
              ]
            });

        @endforeach
    } );
</script>
@endsection