@extends('layouts.admin')

@section('title', 'Sistem Pakar')

@section('content')
<div class="row">
    <div class="col-md-4">
    	<div class="card">
    		<div class="card-header">
    			Data Siswa
    		</div>
    		<div class="card-body">
        		<canvas id="chartSiswa" width="100" height="100"></canvas>
        		<p>Total Siswa : {{count($siswas)}}<br>
        			@foreach($jumlah_siswa as $namaindikator => $jumlah)
        			{{ $namaindikator }} : {{$jumlah}} <br>
        			@endforeach
        		</p>
    		</div>
    		
    	</div>
    </div>

    <div class="col-md-4">
    	<div class="card">
    		<div class="card-header">
    			Data Pertanyaan
    		</div>
    		<div class="card-body">
        		<canvas id="chartPertanyaan" width="100" height="100"></canvas>
        		<p>Total Pertanyaan : {{count($pertanyaans)}}<br>
        			@foreach($indikators as $indikator)
        			{{ $indikator->nama }} : {{count($indikator->pertanyaans)}} <br>
        			@endforeach
        		</p>
        		<br>
    		</div>
    		
    	</div>
    </div>
</div>
@endsection


@section('toast')
	@parent

    @if (Session::has('authenticated'))
      toastr.success('Selamat datang {{ Auth::user()->name }}')
    @endif
@endsection



@section('css')
@parent
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/chart.js/Chart.min.css') }}">
@endsection

@section('js')
@parent

<script src="{{ asset('adminlte/plugins/chart.js/Chart.min.js') }}"></script>
<script>
var cts = document.getElementById('chartSiswa').getContext('2d');
var chartSiswa = new Chart(cts, {
    type: 'pie',
    data: {
        labels: JSON.parse('{!! json_encode($data_chart_siswa["label"]) !!}'),
        datasets: [{
            label: '# of Votes',
            data: JSON.parse('{!! json_encode($data_chart_siswa["jumlah"]) !!}'),
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
            ],
            borderWidth: 1
        }]
    },
});


var ctp = document.getElementById('chartPertanyaan').getContext('2d');
var chartPertanyaan = new Chart(ctp, {
    type: 'pie',
    data: {
        labels: JSON.parse('{!! json_encode($data_chart_pertanyaan["label"]) !!}'),
        datasets: [{
            label: '# of Votes',
            data: JSON.parse('{!! json_encode($data_chart_pertanyaan["jumlah"]) !!}'),
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
            ],
            borderWidth: 1
        }]
    },
});
</script>

@endsection