@csrf
<div class="form-group">
	<label for="absen" class="control-label">No. Absen</label>
	<input type="number" min="1" class="form-control @error('absen') is-invalid @enderror" id="absen" name="absen" placeholder="Nomor Absen" value="{{  old('absen') }}" required autofocus>
</div>

<div class="form-group">
	<label for="kelas" class="control-label">Kelas</label>
	<input type="text" class="form-control @error('kelas') is-invalid @enderror" id="kelas" name="kelas" placeholder="Kelas" value="{{ old('kelas') }}" required>
</div>

<div class="form-group">
	<label for="nama" class="control-label">Nama</label>
	<input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" placeholder="Nama Lengkap" value="{{  old('nama') }}" required>
</div>

<div class="form-group">
	<label for="email" class="control-label">E-Mail</label>
	<input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="E-mail" value="{{  old('email') }}" required>
</div>

<div class="form-group">
	<label for="kelamin" class="control-label">Kelamin</label>
	<select id="kelamin" name="kelamin" class="form-control @error('kelamin') is-invalid @enderror" required>
		<option value=""></option>
		<option value="L" {!! old('kelamin') == 'L' ? 'selected' : '' !!}>Laki-Laki</option>
		<option value="P" {!! old('kelamin') == 'P' ? 'selected' : '' !!}>Perempuan</option>
	</select>
</div>

<div class="form-group">
	<label for="password" class="control-label">Password</label>
	<input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" 
		{!! \Request::is('admin/siswa/*/edit') ? '' : 'required' !!}>
	@if(\Request::is('admin/siswa/*/edit'))
	<span class="small">Silahkan isi untuk mengganti password, atau biarkan kosong.</span>
	@endif
</div>

<button type="submit" class="btn btn-info">Simpan</button>