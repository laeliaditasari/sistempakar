@extends('layouts.admin')

@section('title')
Siswa : {{ $siswa->nama }}
@endsection

@section('content')
<div>
    <a href="{{ route('siswa.edit', [$siswa]) }}" class="btn btn-primary">Edit siswa</a>
    <a href="{{ route('siswa.destroy', [$siswa]) }}" 
        onclick="event.preventDefault(); if(confirm('Anda yakin ingin menghapus {{ $siswa->nama }} ?')) {
            document.getElementById('delete-siswa-{{ $siswa->id }}').submit();
        }"
        class="btn btn-danger">Hapus siswa</a>

    <form id="delete-siswa-{{ $siswa->id }}" action="{{ route('siswa.destroy', [$siswa]) }}" method="POST" style="display: none;">
        @csrf
        @method('DELETE')
    </form>
</div>
<br>
<div class="row">
    <div class="col-lg-8 col-md-8">
        <table class="table bordered">
            <tr>
                <th>No. Absen :</th>
                <td>{{ $siswa->absen }}</td>
            </tr>
            <tr>
                <th>Nama Siswa :</th>
                <td>{{ $siswa->nama }}</td>
            </tr>
            <tr>
                <th>E-Mail :</th>
                <td>{{ $siswa->email }}</td>
            </tr>
            <tr>
                <th>Jenis Kelamin :</th>
                <td>{!! $siswa->kelamin == 'L' ? 'Laki-Laki' : 'Perempuan' !!}</td>
            </tr>
        </table>
    </div>
</div>
@endsection