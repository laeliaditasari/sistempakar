@extends('layouts.admin')

@section('title')
Data Siswa (<small class="text-gray">Total : {{ count($siswas) }}</small>)
@endsection

@section('content')
<div>
    <a href="{{ route('siswa.create') }}" class="btn btn-success">Tambah Siswa</a>
</div>
<br>
<div class="card card-info">
    <div class="card-header">
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No. Absen</th>
                            <th>Nama</th>
                            <th>Kelamin</th>
                            <th style="width: 150px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach ($siswas as $siswa)
                    <tr>
                        <td>{{ $siswa->absen }}</td>
                        <td>{{ $siswa->nama }}</td>
                        <td>{{ $siswa->kelamin }}</td>
                        <td>
                            <a href="{{ route('siswa.show', [$siswa]) }}" class="btn btn-sm btn-success"><i class="fas fa-eye"></i></a>
                            <a href="{{ route('siswa.edit', [$siswa]) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                            <a href="{{ route('siswa.destroy', [$siswa]) }}" 
                                onclick="event.preventDefault(); if(confirm('Anda yakin ingin menghapus siswa : {{ $siswa->nama }} ?')) {
                                    document.getElementById('delete-siswa-{{ $siswa->id }}').submit();
                                }"
                                class="btn btn-danger btn-sm">
                                <i class="fas fa-trash-alt"></i>
                            </a>

                            <form id="delete-siswa-{{ $siswa->id }}" action="{{ route('siswa.destroy', [$siswa]) }}" method="POST" style="display: none;">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--End Card Body-->
</div>
@endsection


@section('css')
@parent
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.min.css') }}">
@endsection

@section('js')
@parent
    <!-- DataTable -->
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
          "columnDefs": [
            { "orderable": false, "targets": 3 }
          ]
        });
    } );
</script>
@endsection
