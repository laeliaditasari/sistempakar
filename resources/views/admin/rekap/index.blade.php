@extends('layouts.admin')

@section('title', 'Rekap Data')

@section('content')
<div>
    <a href="{{ route('rekap.print') }}" class="btn btn-info"><i class="fas fa-print"></i> Cetak Data</a>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        @foreach($indikators as $indikator)
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">
                <small>User dengan Indikator</small> : <b>{{$indikator->nama}} <small>({{count($indikator->siswas)}})</small></b>
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if(count($indikator->siswas) > 0)
                <table id="datatable-{{$indikator->nama}}" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Siswa</th>
                        <th>E-mail</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($indikator->siswas as $siswa)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $siswa->nama }}</td>
                        <td>{{ $siswa->email }}</td>
                        <td>
                            <a href="{{ route('rekap.show', [$siswa]) }}" class="btn btn-sm btn-success"><i class="fas fa-eye"></i></a>
                            <a href="{{ route('rekap.destroy', [$siswa]) }}" 
                                onclick="event.preventDefault(); if(confirm('Anda yakin ingin menghapus hasil ini ?')) {
                                    document.getElementById('delete-jawaban-siswa-{{ $siswa->id }}').submit();
                                }"
                                class="btn btn-danger btn-sm">
                                <i class="fas fa-trash-alt"></i>
                            </a>

                            <form id="delete-jawaban-siswa-{{ $siswa->id }}" action="{{ route('rekap.destroy', [$siswa]) }}" method="POST" style="display: none;">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                <div class="text-center">
                    -
                </div>
                @endif
            </div>
            <!-- /.card-body -->
        </div>
        <br>
        <br>
        @endforeach
    </div>
</div>
@endsection


@section('css')
@parent
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.min.css') }}">
@endsection

@section('js')
@parent
    <!-- DataTable -->
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        @foreach($indikators as $indikator)
            $('#datatable-{{$indikator->nama}}').DataTable({
              "columnDefs": [
                { "orderable": false, "targets": 3 }
              ]
            });

        @endforeach
    } );
</script>
@endsection
