<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="
http://www.w3.org/1999/xhtml">
<head>
	<title>Rekap Data Sistem Pakar Gaya Belajar Siswa</title>
</head>
<body>
	<h1 style="font-size: 18px;">Rekap Data Sistem Pakar Gaya Belajar Siswa</h1>
	@foreach($indikators as $indikator)	

		<p>
			Data Siswa dengan gaya belajar <u>{{$indikator->nama}}</u> <br>
			Jumlah Siswa : {{count($indikator->siswas)}}
		</p>

		<table style="width: 100%; border: solid 1px #eee; border-collapse: collapse;" border="1">
			<thead>
				<tr>
					<th>No</th>
					<th>Absen</th>
					<th>Kelas</th>
					<th>Nama Siswa</th>
					<th>E-mail</th>
					<th>Jenis Kelamin</th>
				</tr>
			</thead>

			<tbody>
				<tr>
				@foreach($indikator->siswas as $siswa)

				<td>{{ $loop->iteration }}</td>
				<td>{{ $siswa->absen }}</td>
				<td>{{ $siswa->kelas }}</td>
				<td>{{ $siswa->nama }}</td>
				<td>{{ $siswa->email }}</td>
				<td>{{ $siswa->kelamin }}</td>

				@endforeach
				</tr>
			</tbody>
		</table>
		<br>

	@endforeach
</body>
</body>
</html>