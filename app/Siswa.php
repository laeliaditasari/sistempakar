<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Siswa extends Authenticatable
{
    protected $fillable = ['absen', 'nama', 'email', 'password','kelamin'];
    
    public function jawabans()
    {
        return $this->hasMany('App\Jawaban');
    }


    //PINDAH KE CONTROLLER
    // public function gayaBelajar(){
    // 	$jawabans = $this->jawabans->with('pertanyaan');

    // 	if (count($jawabans) > 0) {
    // 		foreach ($jawabans as $jawaban) {
    // 			return false;
    // 		}
    // 	}
    // }
}
