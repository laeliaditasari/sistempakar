<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;
use App\Jawaban;
use App\Indikator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    /**
     * Menampilkan Halaman Utama Aplikasi.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.index');
    }

    /**
     * Menampilkan Halaman Utama Aplikasi.
     *
     * @return \Illuminate\Http\Response
     */
    public function tes()
    {
        $pertanyaans = Pertanyaan::all();
        return view('main.tes', [
            'pertanyaans' => $pertanyaans,
        ]);
    }

    /**
     * Menyimpan Hasil Tes User.
     *
     * @return \Illuminate\Http\Response
     */
    public function simpanTes(Request $request)
    {
        $siswa = Auth::guard('siswa')->user();
        
        $inputan_jawaban = [];

        foreach ($request->input('jawaban') as $pertanyaan_id => $nilai_jawaban) {

            $inputan_jawaban[] = [
                'siswa_id' => $siswa->id,
                'pertanyaan_id' => $pertanyaan_id,
                'jawaban' => $nilai_jawaban
            ];
        }
        $siswa->jawabans()->delete();
        if($siswa->jawabans()->createMany($inputan_jawaban)){
            return redirect(route('siswa.hasil-tes'));
        }
    }

    /**
     * Menampilkan Hasil Tes User.
     *
     * @return \Illuminate\Http\Response
     */
    public function hasilTes(Request $request)
    {
        $siswa = Auth::guard('siswa')->user();

        $jml_pertanyaan = DB::table('pertanyaans')
                     ->select(DB::raw('indikator_id, sifat, count(*) as jumlah, (count(*) * 4) nilai_max'))
                     ->groupBy('sifat', 'indikator_id')
                     ->get();

        $jml_jawaban = DB::table('jawabans')
                     ->select(DB::raw('indikator_id, sifat, SUM(jawaban) nilai'))
                     ->join('pertanyaans', 'jawabans.pertanyaan_id', '=', 'pertanyaans.id')
                     ->where('siswa_id', $siswa->id)
                     ->groupBy('indikator_id', 'sifat')
                     ->get();

        //HITUNG PERSENTASE
        $persentase = [];
        foreach ($jml_pertanyaan as $val) {
            if (!isset($persentase[$val->indikator_id]['positif'])) {
                $persentase[$val->indikator_id]['positif'] = 0;
            }

            if (!isset($persentase[$val->indikator_id]['negatif'])) {
                $persentase[$val->indikator_id]['negatif'] = 0;
            }

            $persentase[$val->indikator_id][$val->sifat] = $val->nilai_max;
        }

        foreach ($jml_jawaban as $val) {
			$nilai_maximal = $persentase[$val->indikator_id][$val->sifat];
			$nilai_siswa = $val->nilai;
            $persentase[$val->indikator_id][$val->sifat] = ($nilai_siswa * 100) / $nilai_maximal;
        }
        //END HITUNG PERSENTASI
		


        //MENENTUKAN INDIKATOR (GAYA BELAJAR)
        //step 1 tentukan nilai positif tertinggi
        $indikator_id_pos_tertinggi = NULL;
        $pos = 0;
        foreach ($persentase as $id_indikator => $val) {
            if ($val['positif'] > $pos) {
                $indikator_id_pos_tertinggi = $id_indikator;
                $pos = $val['positif'];
            }
        }

        //step 2 bandingkan nilai positif & negatif
        $indikator = Indikator::find($indikator_id_pos_tertinggi);
        $siswa->indikator_id = $indikator->id;
        $siswa->save();


        return view('main.hasil-tes', ['indikator' => $indikator, 'persentase' => $persentase[$indikator_id_pos_tertinggi]]);
    }
}
