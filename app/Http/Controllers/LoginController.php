<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;

use App\Siswa;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:siswa')->except('logout');
    }

    use AuthenticatesUsers;

    protected $redirectTo = '/';
    protected $logoutRedirectTo = '/login';

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('siswa');
    }





    /**
     * Menampilkan Form Untuk Login Siswa.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('main.login');
    }

    /**
     * Validasi login Siswa.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function siswaLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

        if ($this->guard()->attempt($this->credentials($request), false)) {
            $request->session()->flash('toast', 'Selamat Datang '.Auth::guard('siswa')->user()->nama);
            return $this->sendLoginResponse($request);
        }else{
            return $this->sendFailedLoginResponse($request);
        }
    }

    /**
     * Menampilkan Form Untuk Pendaftaran Siswa Baru.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('main.register');
    }

    /**
     * Simpan Siswa Register dan langsung login.
     *
     * @return \Illuminate\Http\Response
     */
    public function siswaRegister(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:siswas'],
            'password' => ['required', 'string', 'confirmed'],
            'kelamin' => ['required'],
            'absen' => ['required']
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);
        $siswa = Siswa::create($validatedData);

        if($this->guard()->attempt($request->only('email','password'), false)){
            $request->session()->flash('toast', 'Selamat Datang '.$siswa->nama);
            return redirect($this->redirectTo);
        }
    }

    /**
     * Logout siswa.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect($this->logoutRedirectTo);
    }

}
