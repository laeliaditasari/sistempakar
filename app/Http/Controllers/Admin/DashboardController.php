<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Siswa;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$siswas = \App\Siswa::all();
    	$indikators = \App\Indikator::all();
    	$pertanyaans = \App\Pertanyaan::all();

    	foreach ($indikators as $indikator) {
    	 	$jumlah_siswa[$indikator->nama] = count($indikator->siswas);
    	 	$data_chart_pertanyaan['label'][] = $indikator->nama;
    	 	$data_chart_pertanyaan['jumlah'][] = count($indikator->pertanyaans);
    	}
    		$jumlah_siswa['Belum Memilih'] = count(Siswa::where('indikator_id', NULL)->get());


    	foreach ($jumlah_siswa as $nama_indikator => $jumlah) {
    		$data_chart_siswa['label'][] = $nama_indikator;
    		$data_chart_siswa['jumlah'][] = $jumlah;
    	}

        return view('admin.index',[
        	'siswas' => $siswas,
        	'indikators' => $indikators,
        	'pertanyaans' => $pertanyaans,
        	'jumlah_siswa' => $jumlah_siswa,
        	'data_chart_siswa' => $data_chart_siswa,
        	'data_chart_pertanyaan' => $data_chart_pertanyaan
        ]);
    }
}
