<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Indikator;
use App\Siswa;
use PDF;

class RekapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$indikators = Indikator::with('siswas')->get();
        return view('admin.rekap.index', [
            'indikators' => $indikators
        ]);
    }
    /**
     * Generate PDF.
     *
     * @return \Illuminate\Http\Response
     */
    public function printPDF()
    {   
        $indikators = Indikator::with('siswas')->get();
        $pdf = PDF::loadView('admin.rekap.pdf', ['indikators' => $indikators]);  
        return $pdf->download('rekap.pdf');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        $indikators = Indikator::with('siswas')->get();
        return view('admin.rekap.index', [
            'indikators' => $indikators
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Siswa $siswa)
    {
        $siswa = $siswa->find($id)->first();
        
        $siswa->jawabans()->delete();
        $siswa->indikator_id = NULL;
        $siswa->save();


        return redirect(route('rekap.index'));
    }
}
