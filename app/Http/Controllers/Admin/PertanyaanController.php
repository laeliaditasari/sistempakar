<?php

namespace App\Http\Controllers\Admin;

use App\Pertanyaan;
use App\Indikator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indikators = Indikator::all();
        return view('admin.pertanyaan.index', [
            'indikators' => $indikators
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $indikators = Indikator::all();
        return view('admin.pertanyaan.create', ['indikators' => $indikators]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'indikator_id' => 'required',
            'pertanyaan' => 'required',
            'sifat' => 'required|in:positif,negatif'
        ]);
        $pertanyaan = Pertanyaan::create($validatedData);

        if ($pertanyaan) {

            $request->session()->flash('toast', 'Tambah Pertanyaan Berhasil');
            return redirect( route('pertanyaan.show', [$pertanyaan]) );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function show(Pertanyaan $pertanyaan)
    {
        return view('admin.pertanyaan.show', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pertanyaan $pertanyaan)
    {
        $indikators = Indikator::all();
        return view('admin.pertanyaan.edit', [
            'pertanyaan' => $pertanyaan,
            'indikators' => $indikators
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pertanyaan $pertanyaan)
    {
        $validatedData = $request->validate([
            'indikator_id' => 'required',
            'pertanyaan' => 'required',
            'sifat' => 'required|in:positif,negatif'
        ]);

        if ($pertanyaan->update($validatedData)) {
            $request->session()->flash('toast', 'Edit Pertanyaan Berhasil');
            return redirect( route('pertanyaan.show', [$pertanyaan]) );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pertanyaan $pertanyaan)
    {
        if ($pertanyaan->delete()) {
            return redirect( route('pertanyaan.index') );
        }else{
            abort(500, 'Delete Pertanyaan '.$pertanyaan->id.' Error. Hubungi Administrator');
        }
    }
}
