<?php

namespace App\Http\Controllers\Admin;

use App\Indikator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Illuminate\Support\Str;

class IndikatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indikators = Indikator::all();
        return view('admin.indikator.index', [
            'indikators' => $indikators
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.indikator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:191',
            'deskripsi' => 'required',
			'saran' => 'required'
        ]);
        $indikator = Indikator::create($validatedData);

        if ($indikator) {
            $request->session()->flash('toast', 'Tambah Indikator Berhasil');
            return redirect( route('indikator.show', [$indikator]) );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Indikator  $indikator
     * @return \Illuminate\Http\Response
     */
    public function show(Indikator $indikator)
    {
        return view('admin.indikator.show', ['indikator' => $indikator]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Indikator  $indikator
     * @return \Illuminate\Http\Response
     */
    public function edit(Indikator $indikator)
    {
        return view('admin.indikator.edit', ['indikator' => $indikator]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Indikator  $indikator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Indikator $indikator)
    { 
        $validatedData = $request->validate([
            'nama' => 'required|max:191',
            'deskripsi' => ''
        ]);

        if ($indikator->update($validatedData)) {
            $request->session()->flash('toast', 'Edit Indikator Berhasil');
            return redirect( route('indikator.show', [$indikator]) );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Indikator  $indikator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Indikator $indikator)
    {
        if ($indikator->delete()) {
            return redirect( route('indikator.index') );
        }else{
            abort(500, 'Delete Indikator '.$indikator->name.' Error. Hubungi Administrator');
        }
    }
}
