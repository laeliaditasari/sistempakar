<?php

namespace App\Http\Controllers\Admin;

use App\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswas = Siswa::all();
        return view('admin.siswa.index', ['siswas' => $siswas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.siswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:191',
            'absen' => 'required|integer|min:0|max:100',
            'email' => 'required|email|unique:siswas|max:191',
            'kelamin' => 'required|in:L,P',
            'password' => 'required'
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);
        $siswa = Siswa::create($validatedData);

        if ($siswa) {
            $request->session()->flash('toast', 'Tambah Siswa Berhasil');
            return redirect( route('siswa.show', [$siswa]) );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        return view('admin.siswa.show', ['siswa' => $siswa]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa)
    {
        return view('admin.siswa.edit', ['siswa' => $siswa]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:191',
            'absen' => 'required|integer|min:0|max:100',
            'email' => 'required|email|max:191',
            'kelamin' => 'required|in:L,P',
            'password' => 'required'
        ]);

        if ($request->password != '') { //JIKA PASSWORD DI ISI MAKA GANTI PASSWORD
            $validatedData['password'] = Hash::make($validatedData['password']);
        }else{
            $validatedData['password'] = $siswa->password;
        }
        
        if ($siswa->update($validatedData)) {

            $request->session()->flash('toast', 'Edit Siswa Berhasil');
            return redirect( route('siswa.show', [$siswa]) );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa)
    {
        if ($siswa->delete()) {
            return redirect( route('siswa.index') );
        }else{
            abort(500, 'Delete Indikator '.$siswa->name.' Error. Hubungi Administrator');
        }
    }
}
