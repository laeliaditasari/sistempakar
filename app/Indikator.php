<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indikator extends Model
{
    protected $fillable = ['nama', 'deskripsi'];
    public $timestamps = false;


    public function pertanyaans()
    {
        return $this->hasMany('App\Pertanyaan');
    }

    public function siswas()
    {
        return $this->hasMany('App\Siswa');
    }
}
