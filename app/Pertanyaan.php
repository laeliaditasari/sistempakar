<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $fillable = ['indikator_id', 'pertanyaan', 'sifat'];
    public $timestamps = false;

    public function indikator()
    {
        return $this->belongsTo('App\Indikator');
    }

    public function jawabans(){
    	return $this->hasMany('App\Jawaban');
    }
	
	public function getKodeAttribute(){
		return 'C'.$this->id;
	}
}
