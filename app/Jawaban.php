<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
	protected $fillable = ['siswa_id', 'pertanyaan_id', 'jawaban'];

    public function siswa()
    {
        return $this->belongsTo('App\Siswa');
    }

    public function pertanyaan()
    {
        return $this->belongsTo('App\Pertanyaan');
    }
}
